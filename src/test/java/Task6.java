import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Task6 {
    @Test
    void Work6() {

        System.setProperty("webdriver.chrome.driver", "C:/Users/HP/Desktop/chromedriver_win32/chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        //leilatestmail@mail.ru
        //Leila1234

        try {

            webDriver.get("https://etsy.com/");
            WebDriverWait wait2 = new WebDriverWait(webDriver, 20);

            wait2.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#desktop-category-nav > div.wt-bg-white.wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-bar > div > ul > li:nth-child(2) > a")));
            WebElement clothLinkMain = webDriver.findElement(By.cssSelector("#desktop-category-nav > div.wt-bg-white.wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-bar > div > ul > li:nth-child(2) > a"));
            Actions builder = new Actions(webDriver);
            builder.moveToElement(clothLinkMain).build().perform();


            WebElement menLinkMain = webDriver.findElement(By.id("side-nav-category-link-10936"));
            builder.moveToElement(menLinkMain).build().perform();


            WebElement bootsLink = webDriver.findElement(By.id("catnav-l4-11109"));
            bootsLink.click();

            WebDriverWait wait3 = new WebDriverWait(webDriver, 10);

            wait3.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#content > div > div.content.bg-white.col-md-12.pl-xs-1.pr-xs-0.pr-md-1.pl-lg-0.pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-text-black > div.col-group.pl-xs-0.search-listings-group.pr-xs-1 > div:nth-child(2) > div.bg-white.display-block.pb-xs-2.mt-xs-0 > div > div > ul > li:nth-child(1) > div > a")));
            List<WebElement> allLinks = webDriver.findElements(By.className("listing-link"));
            int count = Integer.parseInt(webDriver.findElement(By.cssSelector("#content > div > div.content.bg-white.col-md-12.pl-xs-1.pr-xs-0.pr-md-1.pl-lg-0.pr-lg-0.wt-bb-xs-1 > div > div.mt-xs-2.pl-xs-1.pl-md-4.pl-lg-6.pr-xs-1.pr-md-4.pr-lg-6 > div > span > span > span:nth-child(2)")).getText());
            Assertions.assertEquals(count, allLinks.size());


        } finally {
            webDriver.close();

        }
    }
}
