import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Task5 {
    @Test
    void Work5() {

        System.setProperty("webdriver.chrome.driver", "C:/Users/HP/Desktop/chromedriver_win32/chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        //leilatestmail@mail.ru
        //Leila1234

        try {

            webDriver.get("https://etsy.com/");
            WebDriverWait wait2 = new WebDriverWait(webDriver, 10);

            wait2.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#desktop-category-nav > div.wt-bg-white.wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-bar > div > ul > li:nth-child(2) > a")));
            WebElement clothLinkMain = webDriver.findElement(By.cssSelector("#desktop-category-nav > div.wt-bg-white.wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-bar > div > ul > li:nth-child(2) > a"));
            Actions builder = new Actions(webDriver);
            builder.moveToElement(clothLinkMain).build().perform();


            WebElement menLinkMain = webDriver.findElement(By.id("side-nav-category-link-10936"));
            builder.moveToElement(menLinkMain).build().perform();


            WebElement bootsLink = webDriver.findElement(By.id("catnav-l4-11109"));
            bootsLink.click();
            WebDriverWait wait = new WebDriverWait(webDriver, 10);

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#search-filter-reset-form > div:nth-child(2) > fieldset > div > div > div:nth-child(2) > div > a")));

            WebElement onSaleButton = webDriver.findElement(By.cssSelector("#search-filter-reset-form > div:nth-child(2) > fieldset > div > div > div:nth-child(2) > div > a"));
            onSaleButton.click();
            WebDriverWait wait3 = new WebDriverWait(webDriver, 20);

            wait3.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("n-listing-card__price")));

            List<WebElement> container = webDriver.findElements(By.className("n-listing-card__price"));


        } finally {
            webDriver.close();

        }
    }
}
